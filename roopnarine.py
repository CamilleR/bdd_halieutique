#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 31 10:06:38 2018

@author: camillemac
"""
import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
import random

r=0.292
E=0
q=0.272
k=1195
b0=1000
val=[]
v=[]

def e(t):
    if t==0:
        return 0.9
    else:
        return float(e(t-1))-0.01
#    return random.uniform(0.1, 0.5)
    
def croissance(t):
    return np.exp(1-fonc(t)/k)
    
def fonc(t):
    if t==0:
        return b0
    else:
        return fonc(t-1)*(croissance(t-1) - E)

for i in range(0,20):
    val.append(fonc(i))
    v.append(i)
    print("t= " + str(i) + ": " + str(fonc(i)))
    print(e(i))
    
y= np.array(val)
x= np.array(v)

plt.plot(x, y, label="croissance")


#for i in range(1,20):
#    print(gss(i))

plt.legend()

plt.show()