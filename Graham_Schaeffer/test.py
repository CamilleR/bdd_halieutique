#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  9 14:51:24 2018

@author: camillemac
"""

# -*- coding: utf-8 -*-

#print("hello world")

import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
import random
import csv

val= []
val2=[]
vaal=[]
vaaal=[]
vaaal2=[]
vaaal3=[]
v=[]
err=[]
r=0.292
#E=1
q=0.272
k=1195
b0=1000
#Idee pour reéquilibrer le k: importer le fichier csv, regarder les b0 et extrapoler les k
def e(t):
    #for i in range(0,20):
     #   vaaal.append(random.randint(0, 2))
    #return vaaal[t]
    return 1

def k1(c):
    return (k+k*c)/6


def gs1(t,c):
    if t == 0:
        return b0
    else:
        return k*x1(t)/y21(t-1,c)
    
def y21(t,c):
    #return 1+((((k/6)+(k*c/6))*x1(t)/((gs1(t,c)/6)+gs1(t,c)*c/6))-1)*np.exp(-r*x1(t))    
    return 1+((k*x1(t)/((gs1(t,c)+gs1(t,c)*c)))-1)*np.exp(-r*x1(t))
    
def gss(t):
#    if t==1:
#        c=0
#    else:
    c=random.uniform(-0.15,0.15)
    return gs1(t,c)   
    
      
def graham_schaeffer(t):
    if t == 0:
        return b0
    else:
        return k*x1(t)/y1(t-1)

def graham_schaeffer2(t):
    if t == 0:
        return b0
    else:
        return (k/6)*x1(t)/y2(t-1)

def x1(t):
    return (1-(q*e(t)/r))


def y1(t):
    return 1+((k*x1(t)/graham_schaeffer(t))-1)*np.exp(-r*x1(t))

def y2(t):
    return 1+(((k/6)*x1(t)/(graham_schaeffer(t)/6))-1)*np.exp(-r*x1(t))


for i in range(1,20):
    val.append(graham_schaeffer(i))
    vaal.append(i)
    val2.append(graham_schaeffer2(i))
    v.append(gss(i))
    print(i)
    

y11= np.array(val)
y12= np.array(val2)
y111=np.array(v)
x= np.array(vaal)

plt.plot(x, y11, label="B(t) sur la grande zone")
plt.plot(x,y12, label="B(t) sur une petite zone")
plt.plot(x,y111, label="test avec migration")
print(gs1(2,0.23))
#for i in range(1,20):
#    print(gss(i))

plt.legend()

plt.show()
