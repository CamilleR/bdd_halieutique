var map, layer, infobox;

function GetMap() {
  map = new Microsoft.Maps.Map('#myMap', {
    zoom: 7
  });
  //Load the GeoXml module.
  Microsoft.Maps.loadModule('Microsoft.Maps.GeoXml', function () {
    //Create an instance of the GeoXmlLayer.
    layer = new Microsoft.Maps.GeoXmlLayer();
    //Add the layer to the map.
    map.layers.insert(layer);
    //Set the URL of the geo XML file as the data source of the layer.
  });
}

//Parse the XML data.
function readXml(){
  getSource()
  requestZones(function(data) {
    var dataSet = Microsoft.Maps.GeoXml.read(data);
    console.log(dataSet);
    renderGeoXmlDataSet(dataSet);
  });
}

//Récupère la valeur passée par l'utilisateur
function getSource(){
  var choix=document.fTest.source
  for (i=0; i< choix.length; i++) {
    if (choix[i].checked==true){
      console.log("choix value: " + choix[i].value)
      switch (choix[i].value) {
        case String(choix[i].value):
          return String(choix[i].value)
          break
      }
    }
  }
}

//Trace l'évolution des données, utlisation de Plotly
function draw(x, y=null) {
  try {
    const xValues = math.range(2005,2016, 1).toArray()
    const yValues1 =[]
    const yValues2= []
    for(let i=0; i<=10;i++){
      if(y!=null){
        yValues1.push(parseInt(x[i].captures)+ parseInt(y[i].captures))
        yValues2.push(parseInt(x[i].mesures)+ parseInt(y[i].mesures))
      }
      else{
        yValues1.push(parseInt(x[i].captures))
        yValues2.push(parseInt(x[i].mesures))
      }
    }
    const trace1 = {
      x: xValues,
      y: yValues1,
      type: 'scatter'
    }
    const trace2 = {
      x: xValues,
      y: yValues2,
      type:'scatter'
    }
    const data = [trace1, trace2]
    Plotly.newPlot('plot', data)
  }
  catch (err) {
  console.error(err)
  alert(err)
  }
}

//trace les zones kml
function renderGeoXmlDataSet(data) {
  requestData(function(dat){
    if(data.shapes[0].metadata.id){
      let somme=0
      let desc= null
      d= JSON.parse(dat)
      map.entities.clear();
      map.layers.clear();
      console.log(data.shapes[0].metadata.id.value)
      if(infobox)
        infobox.setMap(null);
      switch (parseInt(data.shapes[0].metadata.id.value)){
        case 1:
          console.log(d.Zones[0].z1[10].captures)
          somme = parseInt(d.Zones[0].z1[10].captures) + parseInt(d.Zones[4].z5[10].captures)
          desc= "\nNombre de port: 9 \nNombre de bateaux: 56"
          draw(d.Zones[0].z1,d.Zones[4].z5)
          break
        case 2:
          somme = parseInt(d.Zones[1].z2[10].captures)
          desc= "\nNombre de port: 4 \nNombre de bateaux: 22"

          draw(d.Zones[1].z2)
          break
        case 3:
          somme = parseInt(d.Zones[2].z3[10].captures) + parseInt(d.Zones[5].z6[10].captures)
          desc= "\nNombre de port: 14 \nNombre de bateaux: 118"
          draw(d.Zones[2].z3,d.Zones[5].z6)
          break
        case 4:
          somme = parseInt(d.Zones[3].z4[10].captures)
          desc= "\nNombre de port: 9 \nNombre de bateaux: 66"


          draw(d.Zones[3].z4)
          break
      }

      infobox = new Microsoft.Maps.Infobox( new Microsoft.Maps.Location(data.summary.bounds.center.latitude,data.summary.bounds.center.longitude), {
        description : "nombre de captures pour l'année 2015: " + somme + desc
      });
      infobox.setMap(map);
    }

    //Add all shapes that are not in layers to the map.
    if (data.shapes) {
      map.entities.push(data.shapes);
    }

    //Add all data layers to the map.
    if (data.layers) {
      for (var i = 0, len = data.layers.length; i < len; i++) {
        map.layers.insert(data.layers[i]);
      }
    }
    //Add all screen overlays to the map.
    if (data.screenOverlays) {
      for (var i = 0, len = data.screenOverlays.length; i < len; i++) {
        map.layers.insert(data.screenOverlays[i]);
      }
    }

    if (data.summary && data.summary.bounds) {
      //Set the map view to focus show the data.
      map.setView({ bounds: data.summary.bounds, padding: 30 });
    }
  })
}

function requestZones(callback){
  var xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function(data) {
    if (this.readyState == 4 && this.status == 200) {
      callback(xhttp.responseText)
    }
  };
  xhttp.open("GET", "/dataZone/" + getSource(), true)
  xhttp.send()
}

function requestData(callback){
  var xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function(data) {
    if (this.readyState == 4 && this.status == 200) {
      callback(xhttp.responseText)
    }
  };
  xhttp.open("GET", "/data/", true)
  xhttp.send()
}
