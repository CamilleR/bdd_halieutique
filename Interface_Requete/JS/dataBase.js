var mysql = require('mysql');
const fs = require('fs')

var mySqlClient = mysql.createConnection({
  "host": "127.0.0.1",
  "port": 3306,
  "database": "BDD_Halieutique",
  "password": "root",
  "name": "db",
  "user": "root",
  "connector": "mysql",
  "socketPath": "/Applications/MAMP/tmp/mysql/mysql.sock"
});
mySqlClient.connect(function(err){
  if (err) throw err;
  console.log("Connected to the database!")
})

function readEspeces(i, st) {
  return new Promise(function (resolve, reject) {
    var captures = []
    selectQuery = 'SELECT nom_commun, Year(date_sortie) AS annee, SUM(nombre) AS nb, COUNT(DISTINCT idMesure) AS nb_mesure, COUNT(DISTINCT idMesure)/SUM(nombre)*100 as nbb, idStrate FROM Embarquement, Engin, Embarquement_has_Engin, Mesure, Espece, Zone, Strate WHERE idEmbarquement = Embarquement_idEmbarquement AND idEngin = Embarquement_has_Engin.Engin_idEngin AND idEngin = Mesure.Engin_idEngin AND idEspece = Espece_idEspece AND idZone= Zone_idZone AND idStrate= Zone_idStrate  AND Year(date_sortie)=' + i + ' AND idStrate=' + st + ' GROUP BY annee, nom_commun ORDER BY `annee` ASC'
    sqlQuery= mySqlClient.query(selectQuery);
    sqlQuery.on("result", function(rows) {
      //console.log(rows)
      captures.push(rows)
      resolve(captures)
    })
  })
}

function readData(st){
  return new Promise(function (resolve, reject) {
    var captures = []
    selectQuery = 'SELECT Year(date_sortie) AS annee, SUM(nombre) AS nb, COUNT(DISTINCT idMesure) as nb_mesure, COUNT(DISTINCT idMesure)/SUM(nombre)*100 as moy, idStrate FROM Embarquement, Engin, Embarquement_has_Engin, Mesure, Espece, Zone, Strate WHERE idEmbarquement = Embarquement_idEmbarquement AND idEngin = Embarquement_has_Engin.Engin_idEngin AND idEngin = Mesure.Engin_idEngin AND idEspece = Espece_idEspece AND idZone= Zone_idZone AND idStrate= Zone_idStrate AND idStrate=' + st + ' AND Year(date_sortie)>2004 GROUP BY annee ORDER BY `annee` ASC'
    sqlQuery= mySqlClient.query(selectQuery);
    sqlQuery.on("result", function(rows) {
      captures.push(rows)
      resolve(captures)
    })
  })
}


function readFileCSV() {
  return new Promise(function (resolve, reject) {
    fs.readFile(__dirname + '/../monfichier.csv', {encoding: 'utf8'}, function (err, data) {
      let values= []
      let lines=[]
      if (err) throw err
      lines = data.split('\n')
      for (let line of lines.slice(3, -1)) {
        values.push(line.split(';'))
      }
      resolve(values)
    })
  })
}

exports.readEspeces = readEspeces
exports.readData = readData
exports.readFileCSV = readFileCSV
