var mysql = require('mysql');
var captures=[]
var promise = require('promise');
var somme=0
var mySqlClient = mysql.createConnection({
     "host": "127.0.0.1",
     "port": 3306,
     "database": "BDD_Halieutique",
     "password": "root",
     "name": "db",
     "user": "root",
     "connector": "mysql",
     "socketPath": "/Applications/MAMP/tmp/mysql/mysql.sock"
});


function readData(i) {
   return new Promise(function (resolve, reject) {
     mySqlClient.connect(function(err) {
           if (err) throw err;
           console.log("Connected to the database!")
             selectQuery = 'SELECT Year(date_sortie) AS annee, idEspece, nom_commun, nom_zone, SUM(nombre) AS nb, COUNT(DISTINCT idMesure) AS nb_mesure, Zone_idZone FROM Embarquement, Engin, Embarquement_has_Engin, Mesure, Espece, Zone WHERE idEmbarquement = Embarquement_idEmbarquement AND idEngin = Embarquement_has_Engin.Engin_idEngin AND idEngin = Mesure.Engin_idEngin AND idEspece = Espece_idEspece AND idZone= Zone_idZone AND Year(date_sortie) = ' + i + ' GROUP BY annee, nom_commun ORDER BY `Espece`.`idEspece` ASC'
             sqlQuery= mySqlClient.query(selectQuery);
             sqlQuery.on("result", function(rows) {
               captures.push(rows)
               resolve(captures)
             })
     })
   })
}

function getData(i)
{
  readData(i).then(function (data) {
      data.forEach( function(d){
        somme += d.nb
      })
      console.log("Nombre de prises pour l'année " + i + " : " + somme)
    });
}

/*var captures = []
var somme = 0
var mysql = require('mysql');
var promise = require('promise');

var mySqlClient = mysql.createConnection({
     "host": "127.0.0.1",
     "port": 3306,
     "database": "BDD_Halieutique",
     "password": "root",
     "name": "db",
     "user": "root",
     "connector": "mysql",
     "socketPath": "/Applications/MAMP/tmp/mysql/mysql.sock"
});

function addsomme(nb){
  somme += nb
}
function getData(){
    mySqlClient.connect(function(err) {
          if (err) throw err;
          console.log("Connected to the database!");
          for(let i=1; i<=177; i++){
            selectQuery = 'SELECT Year(date_sortie), nom_prudhomie, SUM(nombre) AS nb FROM Embarquement, Embarquement_has_Engin, Engin, Filet, Port, Prudhomie, Mesure WHERE idEmbarquement = Embarquement_idEmbarquement AND idEngin = Embarquement_has_Engin.Engin_idEngin AND idEngin = Filet.Engin_idEngin AND idEngin = Mesure.Engin_idEngin AND idPort = Port_idPort AND idPrudhomie = Prudhomie.idPrudhomie AND Year(date_sortie) = 2013 AND Espece_idEspece = ' + i + ' GROUP BY Year(date_sortie), nom_prudhomie'
            sqlQuery= mySqlClient.query(selectQuery);

            sqlQuery.on("result", function(rows) {
              console.log(JSON.stringify(rows));
              captures.push(Number.parseInt(rows.nb))
            });
              /*captures.push(Number.parseInt(res.nb))
              somme += Number.parseInt(res.nb)
          }
    });
}


function showData(mySqlClient){

}
readData()

exports.readData = readData
exports.getData = getData*/
