# MySQL Workbench Plugin
# <description>
# Written in MySQL Workbench 6.3.4

from wb import *
import grt
import mforms

ModuleInfo = DefineModule("ModelDocumentation", author="ARAUJO David", version="1.0.0", description="Generate HTML documentation from a model")

# This plugin takes no arguments
@ModuleInfo.plugin("info.da.wb.documentation", caption="Generate documentation (HTML)", description="description", input=[wbinputs.currentDiagram()], pluginMenu="Utilities")
@ModuleInfo.export(grt.INT, grt.classes.db_Catalog)
def documentation(diagram):

	s = sorted(diagram.figures, key=lambda figure: figure.name)

	text = "<head>\n\t<link rel=\"stylesheet\" href=\"../css/retro.css\">\n<link rel=\"stylesheet\" href=\"../css/test.css\">\n<link rel=\"stylesheet\" href=\"../css/menu.css\">\n\t<title>DataBase documentation</title>\n</head>\n<body>\n\t"

	text += "<div id=\"leftcol\"><blockquote><h3><ul id=\"menu-vertical\">"

	for figure in s:
		if hasattr(figure, "table") and figure.table:
			text += "<li>" + writeMenu(figure.table) + "</li>"

	text += "</ul></h3></blockquote></div><div id=\"maincol\">\n"

	text += "<h1>DataBase documentation</h1><br>\n\t"

	text += "<hr><br>\n\t"

	for figure in s:
		if hasattr(figure, "table") and figure.table:
			text += "\t<blockquote>\n\t\t" + writeTableDoc(figure.table) + "\n\t</blockquote>\n\t<br><hr><br>\n"

	text += "</div></body>"

	path = "/home/user/Alternance/MoonFish/bdd_halieutique/interface_requete/doc/doc.html"
	fichier = open(path,"w")
	fichier.write(text)
	fichier.close()
	mforms.App.get().set_status_text("Documentation generated at " + path)

	print "Documentation created."
	return 0

def writeMenu(table):
	text = "<a href=\"#" + table.name + "\" data-original-href=\"#" + table.name + "\" ><code>" + table.name + "</code></a>\n\t"
	return text

def writeTableDoc(table):
	text = "<h2> Table: <a name=\"" + table.name +"\"></a> <code>" + table.name + "</code></h2>\n\t\t"

	text += "<h3> Description: </h3>\n\t\t"

	text += table.comment + "\n\t\t"

	text += "<h3> Columns: </h3>\n\t\t"

	text += "<table>\n\t\t\t<thead>\n\t\t\t\t<tr>\n\t\t\t\t\t<th> Column </th>\n\t\t\t\t\t<th> Data type </th>\n\t\t\t\t\t<th> Attributes </th>\n\t\t\t\t\t<th> Default </th>\n\t\t\t\t\t<th> Description </th>\n\t\t\t\t</tr>\n\t\t\t</thead>\n\t\t\t<tbody>\n\t\t\t\t"

	for column in table.columns:
		text += writeColumnDoc(column, table)

	text += "\n\t\t\t</tbody>\n\t\t</table><br>\n\t\t"

	if (len(table.indices)):
		text += "<h3> Indices: </h3>\n\t\t"

		text += "<table>\n\t\t\t<thead>\n\t\t\t\t<tr>\n\t\t\t\t\t<th> Name </th>\n\t\t\t\t\t<th> Columns </th>\n\t\t\t\t\t<th> Type </th>\n\t\t\t\t\t<th> Description </th>\n\t\t\t\t</tr>\n\t\t\t</thead>\n\t\t\t<tbody>\n\t\t\t\t"

		for index in table.indices:
			text += writeIndexDoc(index)

	text += "\n\t\t\t</tbody>\n\t\t</table>\n"

	return text


def writeColumnDoc(column, table):
	# column name
	text = "<tr>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<code>" + column.name + "</code>\n\t\t\t\t\t\t<a name=\"" + column.name +"\"></a>\n\t\t\t\t\t"

	# column type name
	if column.simpleType:
		text += "</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t" + column.simpleType.name
		# column max lenght if any
		if column.length != -1:
			text += "(" + str(column.length) + ")"
	else:
		text += "\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>"


	text += "\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t"

	# column attributes
	attribs = [];

	isPrimary = False;
	isUnique = False;
	for index in table.indices:
		if index.indexType == "PRIMARY":
			for c in index.columns:
				if c.referencedColumn.name == column.name:
					isPrimary = True
					break
		if index.indexType == "UNIQUE":
			for c in index.columns:
				if c.referencedColumn.name == column.name:
					isUnique = True
					break

	# primary?
	if isPrimary:
		attribs.append("PRIMARY")

	# auto increment?
	if column.autoIncrement == 1:
		attribs.append("Auto increments")

	# not null?
	if column.isNotNull == 1:
		attribs.append("Not null")

	# unique?
	if isUnique:
		attribs.append("Unique")

	text += "\t, ".join(attribs)

	# column default value
	text += "\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t" + (("<code>" + column.defaultValue + "</code>") if column.defaultValue else " ")

	# column description
	text += "\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t" + (nl2br(column.comment) if column.comment else "")

	# foreign key
	for fk in table.foreignKeys:
		if fk.columns[0].name == column.name:
			text +=  ("\t\t<br /><br />" if column.comment else "\t\t") + "<strong>foreign key</strong> to column <a href=\"#" + fk.referencedColumns[0].name + "\" data-original-href=\"#" + fk.referencedColumns[0].name + "\"><code>" + fk.referencedColumns[0].name + "</code></a> on table <a href=\"#" + fk.referencedColumns[0].owner.name + "\" data-original-href=\"#" + fk.referencedColumns[0].owner.name + "\"><code>" + fk.referencedColumns[0].owner.name + "</code></a>.\n\t\t\t\t"
			break


	# finish
	text += "\t</td>\n\t\t\t\t</tr>\n\t\t\t\t"
	return text

def writeIndexDoc(index):

	# index name
	text = "<tr>\n\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t" + index.name

	# index columns
	text += "</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t" + "\t, ".join(map(lambda x: "<a href=\"#" + x.referencedColumn.name + "\" data-original-href=\"#" + x.referencedColumn.name + "\"><code>" + x.referencedColumn.name + "</code></a>\n\t\t\t\t\t", index.columns))

	# index type
	text += "</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t" + index.indexType

	# index description
	text += "\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t" + (nl2br(index.comment) if index.comment else " ")

	# finish
	text += "\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t"

	return text

def nl2br(text):
	return "<br />".join(map(lambda x: x.strip(), text.split("\n")))
