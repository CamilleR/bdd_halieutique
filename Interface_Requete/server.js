var express = require('express');
var path = require('path');
var http = require('http');
var data =require(__dirname + '/JS/dataBase.js')
const fs = require('fs')
const zones= JSON.parse(fs.readFileSync(__dirname + '/Data/Area/dataZones.json', {encoding: 'utf8'}))
var app = express();
app.use(express.static(path.join(__dirname, 'JS')));
app.use(express.static(path.join(__dirname, 'Data/Area')));


app.get('/', function(req, res) {
      res.render('index.ejs');
})

app.get('/doc', function(req,res){
  res.render('doc.ejs');
})

app.get('/map', function(req,res){
  res.render('map.ejs');
})

app.get('/dataCapture/', function(req,res){
  data.readFileCSV().then(function(d){
    console.log(d)
    res.send(d)
  })
})

app.get('/data/', function(req,res){
    res.send(zones)
})

app.get('/dataZone/:requested', function(req, res){
  fs.readFile(__dirname + '/Data/Area/Zone' + req.params.requested + ".kml", {encoding: 'utf8'}, async function (err, data) {
    res.send(data)
  })
})

app.listen(8080);


/*fs.writeFile('DataEspeces.csv', 'nom_commun;annee;nombre_prise;nombre_mesure;donnees;idStrate\n', "UTF-8")
var logStream= fs.createWriteStream('DataEspeces.csv', {'flags': 'a'})
for(let i=2004;i<=2015; i++){
  for(let st=1; st<=6; st++){
    data.readEspeces(i, st).then(function (data){
      var c=0
      data.forEach(function(d){
        //console.log(d)
        c++
        logStream.write(d.nom_commun + ';' + d.annee+ ';' + d.nb + ';' + d.nb_mesure + ';' + d.nbb + ';' + d.idStrate + "\n")
      })
      logStream.write("Nombre d'especes repertoriees pour l'annee " + i + " dans la strate " + st + ": " + c + "\n", "UTF-8")
    })
  }
}*/

/*fs.writeFile('monfichier.json', '{  "Zones": [', "UTF-8")
for(let i= 1; i<=6; i++){
var logStream= fs.createWriteStream('monfichier.json', {'flags': 'a'})
  data.readData(i).then(function (data){

    logStream.write("{" + '"z' + i + '" : [ ')
    data.forEach(function(d){
      let jsson= {
          annee: d.annee,
          captures: d.nb,
          mesures: d.nb_mesure,
          dt: d.moy
      }
      logStream.write(JSON.stringify(jsson) + ",\n")
      //logStream.write('{"annee": "' + d.annee + '","captures": "' + d.nb + '" },' );
    })
    logStream.write(']},')
  })
}
//logStream.write('}]}')*/
